// include file system module
var fs = require("fs");

// read file sample.html
fs.readFile(
	"./copy.txt",
	// callback function that is called when reading file is done
	function(err, data) {
		if (err) throw err;
		// data is a buffer containing file content
		let out = `{"recipes":[${data
			.toString("utf8")
			.replace(/(?:\r\n|\r|\n)/g, ",")}]}`;
		fs.writeFile("./output.json", out, function(err) {
			if (err) throw err;
			console.log("done");
		});
	}
);
